Fork of tabbed - generic tabbed interface
=================================
This contains custom modifications, largely pulled from the [patches available at suckless.org](https://tools.suckless.org/tabbed/patches/).
* Support alpha windows
* Customizable bar height
* Disable drawing tab names
* Hides the bar when only one client is being run.


tabbed is a simple tabbed X window container.

Requirements
------------
In order to build tabbed you need the Xlib header files.

Installation
------------
#### NixOS
Only flake-based system level configurations are supported (although including
flakes-compat would not be difficult).
In system-level flake.nix add
```
    inputs.tabbed.url = "gitlab:Meptl/tabbed/master";
```
Then within your configuration add the tabbed package.
```
environment.systemPackages = let
    my-tabbed = tabbed.packages.x86_64-linux.tabbed;
in [
    my-tabbed
];
```

#### Linux
Edit config.mk to match your local setup (tabbed is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install tabbed
(if necessary as root):

    make clean install

Running tabbed
--------------
See the man page for details.
